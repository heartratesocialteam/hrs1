require 'open-uri'
class Users::SessionsController < Devise::SessionsController


  # POST /users/sign_in.json
  # Notes: Here Sign in request is accepted for both normal and facebook.
  def create
    if params[:user][:facebook].present? # For Login through Facebook
      response = HTTParty.get("https://graph.facebook.com/me?fields=name,email,id,picture.type(large),birthday,gender&access_token=#{params[:user][:token]}")
      if response.ok?
        # If Existing user logins through facebook and facebook return the existing email then new user will not be created.
        @user = User.where('facebook_id = (?) OR email = (?)',
                           JSON.parse(response.body)['id'], JSON.parse(response.body)['email']).take || User.new
        if @user.new_record?
          @user.email = JSON.parse(response.body)['email']
          @user.first_name = JSON.parse(response.body)['name']
          @user.gender = JSON.parse(response.body)['gender']
          @user.facebook_token = params[:user][:token]
          @user.images.build(avatar: open(JSON.parse(response.body)['picture']['data']['url']))

          @user.skip_email_validation = true # In case of facebook, email and password fields cannot be mandatory.
          @user.skip_password_validation = true
          @user.save
          render json: {errors: @user.errors}, status: :unprocessable_entity and return unless @user.valid?
        end
      else
        render json: {errors: [response.dig('error', 'message')]}, status: response.code
      end
    else # For Login through Email and Password
      @user = User.find_for_database_authentication(email: params[:user][:email])
      valid = @user.present? && @user.valid_password?(params[:user][:password])
      render json: {
          errors: {
              user: ['Email or password is not valid']},
          message: "Email or password is not valid"
      },
             status: :unauthorized and return unless valid
    end
  end

  # POST /users/renew_auth_token.json
  # Notes: Here the refresh token is regenerated.
  def renew_auth_token
    @user = User.regenerate_auth_token(params[:user][:refresh_token])
    render json: {errors: 'Auth token not regenerated!'}, status: :unprocessable_entity unless @user.present?
  end

  private

  def user_create_params
    params.require(:user).permit(:username, :email, :password)
  end
end
