class Users::RegistrationsController < Devise::RegistrationsController

  # -------------- Filters ------------------------------- #
  before_action :current_user, except: [:create]
  # It takes the current user out of Devise's scope and custom defined current_user does not get overridden.
  skip_before_action :authenticate_scope!

  # POST /users/sign_up.json
  def create
    @user = User.new(sign_up_params)
    @status = @user.save
    render status: :unprocessable_entity unless @status
  end

  private

  # White listed params for Sign up
  def sign_up_params
    params.require(:user).permit(:email, :username, :password, :latitude, :longitude)
  end
end