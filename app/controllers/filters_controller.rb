class FiltersController < ApplicationController

  # -------------- Filters ------------------------------- #
  before_action :current_user, except: [:create]
  before_action :set_user
  before_action :validate_user

  def create
    @filter = @user.create_filter(filter_params)
    render json: {filter: @filter, message: 'Filter settings saved successfully.'}, status: :created
  end

  def show
    render json: {filter: @user.filter}, status: :ok
  end

  def destroy
    @status = @user.filter.destroy
    if @status
      render json: {message: 'Filter cleared successfully.'}, status: :ok
    else
      render json: {errors: {user: @user.errors}, message: "Filter not removed due to #{@user.errors.full_messages.to_sentence}"},
             status: :unprocessable_entity
    end
  end

  private

  def filter_params
    params.require(:user).permit(:distance, location: [], age_range: [], dating: [], purpose: [])
  end

  def set_user
    @user = User.where(id: params[:user_id]).take
    render json: {errors: {user: ['not found.']}}, status: :not_found unless @user and return
  end

  def validate_user
    render json: {errors: {user: ['not allowed.']}}, status: :forbidden and return unless current_user === @user
  end
end
