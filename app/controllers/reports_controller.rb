class ReportsController < ApplicationController

  # -------------- Filters ------------------------------- #
  before_action :current_user
  before_action :set_user

  def create
    @report = current_user.reports.create(reported_id: @user.id)
    render status: :unprocessable_entity and return unless @report
  end

  private

  def set_user
    @user = User.where(id: params[:user_id]).take
    render json: {errors: {user: ['not found.']}}, status: :not_found unless @user and return
  end
end
