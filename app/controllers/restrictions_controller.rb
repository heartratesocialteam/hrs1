class RestrictionsController < ApplicationController

  # -------------- Filters ------------------------------- #
  before_action :current_user
  before_action :set_user, only: [:create, :destroy, :blockers, :blocking]
  before_action :validate_user, only: [:blockers, :blocking]

  # POST /users/:user_id/block.json
  def create
    _blocked = current_user.block(@user)
    if _blocked.valid?
      render json: {message: 'Blocked successfully.'}, status: :ok
    else
      render json: {errors: _blocked.errors}, status: :unprocessable_entity
    end
  end

  # DELETE /users/:user_id/block.json
  def destroy
    if current_user.blocking?(@user)
      current_user.unblock(@user)
      if current_user.blocking?(@user)
        render json: {errors: ['Something unexpected occurred. Blocking unsuccessful!']}, status: :unprocessable_entity
      else
        render json: {message: 'Unblocked successfully.'}, status: :ok
      end
    else
      render json: {errors: ['Already not blocked.']}, status: :not_found and return
    end
  end

  # GET /users/:id/blockers.json
  def blockers
  end

  # GET /users/:id/blocking.json
  def blocking
  end

  private
  def set_user
    @user = User.find_by_id(params[:user_id] || params[:id])
    # if user not found
    render json: {errors: ['Requested resource to work on not found.']}, status: :not_found  and return unless @user
  end

  def validate_user
    render json: {errors: {user: ['not allowed.']}}, status: :forbidden and return unless current_user === @user
  end
end
