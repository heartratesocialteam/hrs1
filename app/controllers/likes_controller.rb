class LikesController < ApplicationController

  # -------------- Filters ------------------------------- #
  before_action :current_user
  before_action :set_user
  before_action :set_entity
  before_action :validate_user, only: [:likers]

  # POST /users/:id/like.json
  # POST /users/:user_id/posts/:post_id/likes.json
  def create
    @entity.likes.where(likable: @entity, liker_id: current_user.id).destroy_all
    @like = @entity.likes.create(liker_id: current_user.id, status: Like::STATUS[:like])
    if @like.valid?
      render json: {like: @like, message: 'Liked successfully.'}, status: :created
    else
      render json: {errors: @like.errors}, status: :unprocessable_entity
    end
  end

  # DELETE /users/:id/like.json
  # For User, this action is working as a DISLIKE not UNLIKE
  # DELETE /users/:user_id/posts/:post_id/likes.json
  def destroy
    @likes = @entity.likes.where(likable: @entity, liker_id: current_user.id)
    render json: {errors: {likable: ['already not liked.']}}, status: :not_found if @entity.is_a?(Post) && !@likes.present?
    if @likes.destroy_all
      @like = @entity.likes.create(liker_id: current_user.id, status: Like::STATUS[:dislike]) if @entity.is_a?(User)
      render json: {like: @like, message: 'disliked successfully.'}, status: :ok
    else
      render json: {errors: {like: ['unsuccessful.']}}, status: :unprocessable_entity
    end
  end

  def likers
    @likers = @entity.likers
  end

  private
  def set_entity
    @entity = if params[:post_id]
                @user.posts.where(id: params[:post_id]).take
              else
                @user
              end
    unless @entity
      render json: {errors: {(params[:post_id] ? :post : :user) => 'not found'},
                    message: "#{params[:post_id] ? 'Post' : 'Post'} not found"},
             status: :not_found and return
    end
  end

  def set_user
    @user = User.where(id: params[:user_id]).take
    render json: {errors: {user: ['not found.']}}, status: :not_found unless @user and return
  end

  def validate_user
    render json: {errors: {user: ['not allowed.']}}, status: :forbidden and return unless current_user === @user
  end
end
