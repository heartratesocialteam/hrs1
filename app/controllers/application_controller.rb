class ApplicationController < ActionController::Base

  # -------------- RequestForgeryProtection -------------- #
  protect_from_forgery with: :exception, unless: -> { request.format.json? }

  # -------------- Filters ------------------------------- #
  skip_before_action :verify_authenticity_token

  # -------------- Instance methods ---------------------- #
  def current_user
    authenticate_or_request_with_http_token do |token, options|
      User.where(id: UserCache.instance.get_user_id_from_token(token)).take
    end
  end
end
