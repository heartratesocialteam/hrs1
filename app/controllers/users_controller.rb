class UsersController < ApplicationController

  # -------------- Filters ------------------------------- #
  before_action :current_user
  before_action :set_user, only: [:update, :matches, :show]
  before_action :validate_user, only: [:update, :matches]
  before_action :sanitize_params, only: [:matches]

  # PUT /users/:id.json
  def update
    @user = current_user
    @status = if params[:user][:current_password].present?
                @user.update_with_password(update_password_params)
              else
                @user.update(update_params)
              end
    render status: :unprocessable_entity and return unless @status
  end

  def show
  end

  def matches
    @matches = @user.matches
  end

  private
  def sanitize_params
    if params[:location].present?
      @location = eval(params[:location])
    end
    if params[:age_range].present?
      @age_range = eval(params[:age_range])
    end
    if params[:dating].present?
      @dating = eval(params[:dating])
    end
    if params[:purpose].present?
      @purpose = eval(params[:purpose])
    end
  end

  def set_user
    @user = User.where(id: params[:id]).take
    render json: {errors: {user: ['not found.']}}, status: :not_found unless @user and return
  end

  def validate_user
    render json: {errors: {user: ['not allowed.']}}, status: :forbidden and return unless current_user === @user
  end

  # White listed params for profile update
  def update_params
    params.require(:user).permit(:email, :password, :password_confirmation,
                                 :first_name, :last_name,
                                 :dob,
                                 :about,
                                 :gender,
                                 :occupation,
                                 :distance, :address,
                                 :relationship_status,
                                 :latitude, :longitude,
                                 dating: [],
                                 age_range: [],
                                 purpose: [],
                                 fitness_activity_ids: [],
                                 eating_habit_ids: [],
                                 images_attributes: [:avatar, :id, :_destroy]
    )
  end

  def update_password_params
    params.require(:user).permit(:password,
                                 :password_confirmation,
                                 :current_password
    )
  end
end