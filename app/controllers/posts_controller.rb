class PostsController < ApplicationController

  # -------------- Filters ------------------------------- #
  before_action :current_user
  before_action :set_user, only: [:create, :show, :index, :destroy]
  before_action :validate_user, only: [:create, :show, :index, :destroy]
  before_action :set_post, only: [:show, :destroy]

  # POST /users/:user_id/posts.json
  def create
    @post = @user.posts.create(post_params.merge(user: @user))
    render status: :unprocessable_entity unless @post.valid?
  end

  # GET /users/:user_id/posts/:id.json
  def show
    render status: :ok
  end

  # GET /users/:user_id/posts.json
  def index
    @current_user = current_user
    @posts = @user.posts.order(updated_at: :desc).page(params[:page])
    render status: :ok
  end

  # DELETE /users/:user_id/posts/:id.json
  def destroy
    if @post.user === @user
      if @post.destroy
        render json: {message: 'Deleted!'}, status: :ok
      else
        render json: {errors: @post.errors}, status: :unprocessable_entity
      end
    else
      render json: {message: 'User not authorized to delete post.'}, status: :unauthorized
    end
  end

  private
  def set_post
    @post = Post.where(id: params[:id]).take
    render json: {errors: ['Post not found']}, status: :not_found and return unless @post
  end

  def set_user
    @user = User.find_by_id(params[:user_id])
    render json: {errors: ['User not found']}, status: :not_found and return unless @user
  end

  def validate_user
    render json: {errors: {user: ["not following #{@user.first_name}."]},
                  mesage: "You are not following #{@user.first_name || 'the person'}"},
           status: :forbidden and return unless (current_user === @user || current_user.following.include?(@user))
  end

  def post_params
    params.require(:post).permit(:content, image_attributes: [:avatar, :id, :_destroy])
  end
end