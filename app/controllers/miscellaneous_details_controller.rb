class MiscellaneousDetailsController < ApplicationController

  # -------------- Filters ------------------------------- #
  before_action :current_user

  # GET /miscellaneous_details.json
  def index
    @eating_habits, @fitness_activities = EatingHabit.all, FitnessActivity.all
  end
end