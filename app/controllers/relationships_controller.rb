class RelationshipsController < ApplicationController

  # -------------- Filters ------------------------------- #
  before_action :current_user
  before_action :set_user, only: [:create, :destroy, :followers, :following]
  before_action :validate_user, only: [:followers, :following]

  # POST /users/:user_id/follow.json
  def create
    @relationship = current_user.follow(@user)
    render json: {errors: @relationship.errors}, status: :unprocessable_entity unless @relationship.valid?
  end

  # DELETE /users/:user_id/follow.json
  def destroy
    if current_user.following?(@user)
      current_user.unfollow(@user)
      if current_user.following?(@user)
        render json: {errors: ['Something unexpected occurred. Following unsuccessful!']}, status: :unprocessable_entity
      else
        render json: {message: 'Un-followed successfully.'}, status: :ok
      end
    else
      render json: {errors: ['Already not following.']}, status: :not_found
    end
  end

  # GET /users/:id/followers.json
  def followers
  end

  # GET /users/:id/following.json
  def following
  end

  private
  def set_user
    @user = User.find_by_id(params[:user_id] || params[:id])
    # if user not found
    render json: {errors: ['Requested resource to work on not found.']}, status: :not_found and return unless @user
  end

  def validate_user
    render json: {errors: {user: ['not allowed.']}}, status: :forbidden and return unless current_user === @user
  end
end
