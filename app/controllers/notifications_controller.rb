class NotificationsController < ApplicationController

  # -------------- Filters ------------------------------- #
  before_action :current_user
  before_action :set_user, only: [:update, :index]
  before_action :validate_user, only: [:update, :index]
  before_action :set_notification, only: [:update]

  # PUT /users/:user_id/notifications/:id.json
  def update
    if @notification.read?
      render json: {errors: ['Notification already read.']}, status: :not_found
    else
      if @notification.update(read_at: Time.now)
        render json: {message: 'Notification marked as read successfully!'}, status: :ok
      else
        render json: {errors: {notification: ['not updated.']}}, status: :unprocessable_entity
      end
    end
  end

  # GET /users/:user_id/notifications.json
  def index
    # render json: {notifications: Notification.unread.where(target: @user).page(params[:page])}, status: :ok
    @notifications = Notification.unread.where(target: @user).order(updated_at: :desc)
    render status: :ok
  end

  private
  def set_user
    @user = User.find_by_id(params[:user_id])
    render json: {errors: {user: ['not found']}}, status: :not_found and return unless @user
  end

  def validate_user
    render json: {errors: {user: ['not allowed.']}}, status: :forbidden and return unless current_user === @user
  end

  def set_notification
    @notification = Notification.for_user(@user).where(id: params[:id]).take
    render json: {errors: {notification: ['not found.']}}, status: :not_found and return unless @notification
  end
end
