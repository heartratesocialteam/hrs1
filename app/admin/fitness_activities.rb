ActiveAdmin.register FitnessActivity do

  permit_params :name, image_attributes: [:avatar]

  form do |f|
    f.inputs 'Item variant Form' do
      f.input :name
      f.semantic_fields_for :image do |m|
        m.input :avatar, as: :file, hint: image_tag(f.object.image.medium_url)
        m.input :_destroy, as: :boolean, required: :false, label: 'Remove image'
      end
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :name
    column :image do |activity|
      if activity.image
        image_tag activity.image.avatar, class: 'activity_image'
      end
    end
    actions
  end

  controller do
    before_action :build_image_first, only: [:create, :edit]

    def build_image_first
      @fitness_activity = FitnessActivity.find_by_id(params[:id]) || FitnessActivity.new
      @fitness_activity.build_image unless @fitness_activity.image.present?
    end
  end
end
