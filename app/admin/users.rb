ActiveAdmin.register User do

  permit_params :email,:username,:first_name, :last_name,:dob,:gender,:occupation,:age_range,:distance,:dating,
                :relationship_status, :purpose, images_attributes: [:avatar]

  index do
    selectable_column
    id_column
    column :email
    column :username
    column :first_name
    column :last_name
    column :dob
    column :gender
    column :occupation
    column :age_range
    column :distance
    column :dating
    column :relationship_status
    column :purpose
    # column :images
    column :created_at
    column :updated_at

    actions
  end
end
