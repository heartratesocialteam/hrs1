class User < ApplicationRecord

  # ----------- Constants --------------------- #
  DATING = %w(male female business) #-> For type of matches required and default is female
  GENDER = %w(male female business)
  PURPOSE = ['dating', 'workout buddies', 'social', 'all of them'] # Purpose for creating profile on HeartRateSocial
  RELATIONSHIP_STATUS = ['single', 'in relationship'] # Relationship status
  PROFILE_PICS_LIMIT = 6 #-> As per application restriction
  MIN_AGE = 14 #-> Minimum age to create profile

  # ----------- Devise Models ----------------- #
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable
  geocoded_by :address

  # ----------- Attribute accessors ----------- #
  attr_accessor :skip_email_validation #-> In case of user from facebook, email is not necessary.
  attr_accessor :skip_password_validation #-> In case of user from facebook, password can't be required field.

  # ----------- Serialization ----------------- #
  serialize :age_range, Array #-> Ex. [20, 30] (It means user wants matches between 20 to 30 age group.)
  serialize :dating, Array #-> Ex. ['male', 'female', 'business']
  serialize :purpose, Array #-> Ex. ['dating', 'workout buddies', 'social', 'all of them']

  # ----------- Associations ------------------ #
  has_many :images, as: :imageable
  has_many :likes, as: :likable #-> User profile can be liked by other users.
  has_many :notifications
  has_many :posts, inverse_of: :user, dependent: :destroy
  has_many :reports, inverse_of: :user, dependent: :destroy
  has_one :filter, inverse_of: :user, :dependent => :destroy

  has_many :active_relationships, class_name: 'Relationship', foreign_key: 'follower_id', dependent: :destroy
  has_many :passive_relationships, class_name: 'Relationship', foreign_key: 'followed_id', dependent: :destroy
  has_many :followers, through: :passive_relationships
  has_many :following, through: :active_relationships, source: :followed

  has_many :active_restrictions, class_name: 'Restriction', foreign_key: 'blocker_id', dependent: :destroy
  has_many :passive_restrictions, class_name: 'Restriction', foreign_key: 'blocked_id', dependent: :destroy
  has_many :blocking, through: :active_restrictions, source: :blocked
  has_many :blockers, through: :passive_restrictions

  has_and_belongs_to_many :eating_habits
  has_and_belongs_to_many :fitness_activities

  accepts_nested_attributes_for :images, allow_destroy: true

  # ----------- Validations ------------------- #
  after_validation :geocode, if: ->(obj) {obj.address.present? and obj.address_changed?}
  validates_uniqueness_of :email, allow_nil: true
  validates_uniqueness_of :username, allow_nil: true, length: {min: 1, maximum: 50}
  validates :gender, inclusion: {in: GENDER}, allow_nil: true
  validates :first_name, length: {minimum: 1, maximum: 50}, allow_nil: true
  # validates :occupation, length: {minimum: 1, maximum: 199}, allow_nil: true, allow_blank: true
  # validates :about, length: {minimum: 1, maximum: 300}, allow_nil: true
  validates :relationship_status, inclusion: {in: RELATIONSHIP_STATUS}, allow_nil: true, allow_blank: true
  validates :images, length: {maximum: PROFILE_PICS_LIMIT, message: "A max of #{PROFILE_PICS_LIMIT} images are allowed!"}
  validate :dob do |user|
    if user.dob
      user.errors.add(:dob, 'Not a valid Date of Birth.') if user.dob >= Time.now
    end
  end
  validate :dating do |_user|
    _user.errors.add(:dating, 'should be of ARRAY type.') unless _user.dating.is_a?(Array)
    _user.errors.add(:dating, 'has some invalid values.') unless (_user.dating - DATING).blank?
  end

  validate :purpose do |_user|
    _user.errors.add(:purpose, 'should be of ARRAY type.') unless _user.dating.is_a?(Array)
    _user.errors.add(:purpose, 'has some invalid values.') unless (_user.purpose - PURPOSE).blank?
  end

  # ----------- Callbacks --------------------- #
  before_create :generate_refresh_token
  before_create :set_defaults

  # ----------- Class Methods ----------------- #
  def self.regenerate_auth_token(refresh_token)
    resource = User.where(refresh_token: refresh_token).take
    resource if resource && resource.update_attributes(refresh_token: resource.generate_token)
  end

  # ----------- Instance Methods -------------- #
  def miscellaneous_details
    {
        eating_habits: eating_habits,
        fitness_activities: fitness_activities
    }
  end

  def generate_token
    SecureRandom.urlsafe_base64
  end

  def generate_auth_token
    auth_token = generate_token
    UserCache.instance.set_token_optimistically(auth_token, self.id)
    auth_token
  end

  # Follows a user.
  def follow(other_user)
    active_relationships.create(followed: other_user)
  end

  # Unfollows a user.
  def unfollow(other_user)
    active_relationships.where(followed: other_user).destroy_all
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)
  end

  # Blocks a user.
  def block(other_user)
    active_restrictions.create(blocked: other_user)
  end

  # Unblock a user.
  def unblock(other_user)
    active_restrictions.where(blocked: other_user).destroy_all
  end

  # Returns true if the current user has blocked the other user.
  def blocking?(other_user)
    blocking.include?(other_user)
  end

  def name
    (first_name || '') + (last_name || '')
  end

  def profile_completed?
    !!first_name
  end

  def likers
    User.where(id: likes.where(likable_type: self.class.name, status: 1).collect(&:liker_id))
  end

  def dislikers
    User.where(id: likes.where(likable_type: self.class.name, status: -1).collect(&:liker_id))
  end

  def liked_post?(post)
    post.likers.include?(self)
  end

  # It means current_user is liked by _user
  def liked?(_user)
    likers.include?(_user)
  end

  def disliked?
    dislikers.include?(_user)
  end

  def reported?(_user)
    !!self.reports.where(reported_id: _user.id).take
  end

  def matches
    location ||=[self.filter&.location&.first || self.latitude, self.filter&.location&.last || self.longitude]
    distance ||= self.filter&.distance || self.distance
    age_range ||= self.filter&.age_range || self.age_range
    dating ||= self.filter&.dating || self.dating
    purpose ||= self.filter&.purpose || self.purpose

    User.near(location, distance)
        .where('id not in (?) and (DATE(dob) between (?) and (?)) and gender in (?)',
               [id, *blocker_ids, *blocking_ids],
               (Time.now - age_range.last.years).to_date, (Time.now - age_range.first.years).to_date,
               dating
        ).select {|_user| ((_user.purpose & purpose) != []) and !_user.likes.where(liker_id: id).present?}
  end

  # ---------- Private Methods ----------------- #
  private
  def generate_refresh_token
    if self.refresh_token.blank?
      begin
        self.refresh_token = generate_token
      end while User.exists?(refresh_token: self.refresh_token)
    end
  end

  def set_defaults
    self.dating = ['female']
    self.age_range = [18, 40]
    self.purpose = ['dating']
  end

  def email_required?
    return false if skip_email_validation
    super
  end

  def password_required?
    return false if skip_password_validation
    super
  end
end