class Filter < ApplicationRecord

  # ----------- Serialization ----------------- #
  serialize :location
  serialize :age_range
  serialize :dating
  serialize :purpose

  # ----------- Associations ------------------ #
  belongs_to :user, inverse_of: :filter
end
