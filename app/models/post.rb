class Post < ApplicationRecord

  # ----------- Associations ------------------ #
  belongs_to :user, inverse_of: :posts
  has_many :likes, as: :likable
  # has_many :likers, through: :likes, primary_key: 'liker_id'
  has_one :image, as: :imageable
  accepts_nested_attributes_for :image
  has_many :notifications, -> { where(notify_type: 'Post') }, foreign_key: 'notify_id', inverse_of: :post, dependent: :destroy

  # ----------- Validations ------------------- #
  validates :content, length: {minimum: 2, maximum: 140}, allow_nil: true

  # ----------- Callbacks --------------------- #
  after_create :notify_create

  paginates_per 20

  def likers
    User.where(id: likes.collect(&:liker_id))
  end

  private
  # user_object ->  User whose action created notification
  # target ->       User who should be notified for performed action
  # notify_type ->  Action Performed
  def notify_create
    user.followers.each do |_follower|
      user.notifications.create(
          target: _follower,
          notify_type: self.class.name,
          notify_id: id,
          notify_action: 'create',
          message: "#{user.first_name || user.email || user.phone} has a new post."
      )
    end
  end
end
