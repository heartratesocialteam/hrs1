class EatingHabit < ApplicationRecord

  # ----------- Associations ------------------ #
  has_and_belongs_to_many :users
  has_one :image, as: :imageable
  accepts_nested_attributes_for :image, allow_destroy: true

  # ----------- Validations ------------------- #
  validates :name, length: {minimum: 2, maximum: 50}, presence: true, uniqueness: true
end
