class Image < ApplicationRecord

  # ----------- Constants --------------------- #
  IMAGE_VARIATIONS = {:large => '960x640>', :medium => '300x300>', :thumb => '150x150>', :stamp => '30x30>'}

  # ----------- Associations ------------------ #
  belongs_to :imageable, polymorphic: true, optional: true
  has_attached_file :avatar, styles: IMAGE_VARIATIONS, default_url: '/images/:style/missing.png'

  # ----------- Delegates -------------------- #
  delegate :url, to: :avatar
  IMAGE_VARIATIONS.keys.each do |image_type|
    delegate "#{image_type.to_s}_url".to_sym, to: :avatar
    define_method("#{image_type.to_s}_url") { self.avatar.send(:url, image_type) }
  end

  # ----------- Validations ------------------- #
  validates_attachment_content_type :avatar, content_type: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']

  # Provides all different sizes urls
  def urls
    _urls = Hash.new
    IMAGE_VARIATIONS.keys.each do |image_size|
      _urls.merge!(image_size => avatar.send(:url, image_size))
    end
    _urls
  end
end