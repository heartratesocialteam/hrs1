class Relationship < ApplicationRecord

  # ----------- Associations ------------------ #
  belongs_to :follower, class_name: 'User'
  belongs_to :followed, class_name: 'User'
  has_many :notifications, as: :target

  # ----------- Validations ------------------- #
  validates :follower_id, presence: true
  validates :followed_id, presence: true
  validates_uniqueness_of :follower_id, scope: [:followed_id]
  validate do |relationship|
    if relationship.follower_id == relationship.followed_id
      relationship.errors.add(:followed_id, 'cannot follow own profile.')
    end
  end

  # ----------- Callbacks --------------------- #
  after_create :notify_create

  private
  # user_object ->  User whose action created notification
  # target      ->  User who should be notified for performed action
  # notify_type ->  Action Performed
  def notify_create
    follower.notifications.create(
        target: followed,
        notify_type: 'User',
        notify_id: follower.id,
        notify_action: 'follow',
        message: %{#{follower.first_name || follower.email || follower.phone} started following you.}
    )
  end
end