class Like < ApplicationRecord

  # ----------- Constants --------------------- #
  STATUS = {
      like: 1,
      dislike: -1
  }

  # ----------- Associations ------------------ #
  belongs_to :likable, polymorphic: true

  # ----------- Validations ------------------- #
  validates_uniqueness_of :likable_type, scope: [:likable_id, :liker_id]

  # ----------- Callbacks --------------------- #
  after_create :notify_create

  private
  # user_object ->  User whose action created notification
  # target ->       User who should be notified for performed action
  # notify_type ->  Action Performed
  def notify_create
    _liker = User.where(id: liker_id).take
    _target = likable.is_a?(Post) ? likable.user : likable
    _liker.notifications.create(
        target: _target,
        notify_type: likable.class.name,
        notify_id: likable_id,
        notify_action: "like #{likable.class.name.downcase}",
        message: "#{_liker.first_name || _liker.email || _liker.phone} liked your #{likable.class.name == 'Post' ? 'post' : 'profile'}."
    )
  end
end
