class Restriction < ApplicationRecord

  # ----------- Associations ------------------ #
  belongs_to :blocker, class_name: 'User'
  belongs_to :blocked, class_name: 'User'

  # ----------- Validations ------------------- #
  validates :blocker_id, presence: true
  validates :blocked_id, presence: true
  validates_uniqueness_of :blocker_id, scope: [:blocked_id]
  validate do |restriction|
    if restriction.blocker_id == restriction.blocked_id
      restriction.errors.add(:blocker_id, 'cannot block own profile.')
    end
  end
end
