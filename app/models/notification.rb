class Notification < ApplicationRecord

  # ----------- Serialization ----------------- #
  serialize :target_object
  serialize :user_object
  serialize :notify_object

  # ----------- Associations ------------------ #
  belongs_to :user, optional: true
  belongs_to :target, polymorphic: true, optional: true
  belongs_to :post, inverse_of: :notifications, foreign_key: 'notify_id'

  # ----------- Associations ------------------ #
  scope :unread, -> { where(read_at: nil) }
  scope :read, -> { where.not(read_at: nil) }
  scope :for_user, ->(user) { where(target: user) }

  # ----------- Pagination -------------------- #
  paginates_per 20

  def read?
    self.read_at.present?
  end
end
