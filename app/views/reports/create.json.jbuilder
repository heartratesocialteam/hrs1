json.partial! 'users/user_unwrapped', user_obj: @user, message: 'User reported successfully!'
json.is_reported current_user.reported?(@user)