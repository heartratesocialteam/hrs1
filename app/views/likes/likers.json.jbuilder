json.likers @likers do |liker|
  json.partial! 'users/user_unwrapped', user_obj: liker
  json.is_following @user.following.include?(liker)
  json.is_follower @user.followers.include?(liker)
  json.is_liked liker.liked?(@user)
  json.is_reported @user.reported?(liker)
  json.is_blocked @user.blocking?(liker)
end