json.notifications @notifications do |notification|
  json.id notification.id
  json.user_object do
    json.partial! 'users/user_unwrapped', user_obj: notification.user
    json.is_following notification.target.following.include?(notification.user)
    json.is_follower notification.target.followers.include?(notification.user)
    json.is_liked notification.user.liked?(notification.target)
    json.is_disliked notification.user.liked?(notification.target)
    json.is_reported notification.target.reported?(notification.user)
    json.is_blocked notification.target.blocking?(notification.user)
  end
  json.notify_type notification.notify_type
  json.notify_id notification.notify_id
  json.notify_action notification.notify_action
  json.notify_object do
    json.partial! "#{notification.notify_type.downcase.pluralize}/#{notification.notify_type.downcase}_unwrapped",
                  "#{notification.notify_type.downcase}_obj".to_sym =>
                      notification.notify_type.constantize.where(id: notification.notify_id).take

  end
  json.message notification.message
  json.created_at notification.created_at
  json.updated_at notification.updated_at
end