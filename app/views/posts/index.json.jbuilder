json.posts @posts do |post|
  json.(post, :id, :content, :likes,:created_at, :updated_at)
  json.likes_count post.likes.count
  json.image post.image, :id, :avatar_file_name, :avatar_content_type, :created_at, :urls if !!post.image
  json.is_liked @current_user.liked_post?(post)
end