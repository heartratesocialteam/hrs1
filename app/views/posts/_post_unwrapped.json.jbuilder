json.(post_obj, :id, :content, :likes, :created_at, :updated_at)
json.likes_count post_obj.likes.count
json.is_liked current_user.liked_post?(post_obj)
json.image post_obj.image, :id, :avatar_file_name, :avatar_content_type, :created_at, :updated_at, :urls
json.partial! 'users/user',user_obj: @user