if @post.valid?
  json.partial! 'posts/post', post_obj: @post, message: 'Posted!'
else
  json.partial! 'posts/error', post_obj: @post, message: 'Not Posted!'
end