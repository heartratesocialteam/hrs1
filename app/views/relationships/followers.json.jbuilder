json.followers @user.followers do |match|
  json.partial! 'users/user_unwrapped', user_obj: match
  json.is_following @user.following.include?(match)
  json.is_follower @user.followers.include?(match)
  json.is_liked match.liked?(@user)
  json.is_disliked match.liked?(@user)
  json.is_reported @user.reported?(match)
  json.is_blocked @user.blocking?(match)
end