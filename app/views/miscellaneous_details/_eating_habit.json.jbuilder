json.id eating_habit.id
json.name eating_habit.name
json.created_at eating_habit.created_at
json.updated_at eating_habit.updated_at
if eating_habit.image.present?
  json.image do
    json.avatar_file_name eating_habit.image.avatar_file_name
    json.avatar_content_type eating_habit.image.avatar_content_type
    json.created_at eating_habit.image.created_at
    json.updated_at eating_habit.image.updated_at
    json.urls eating_habit.image.urls
  end
else
  json.image eating_habit.image
end