json.eating_habits @eating_habits do |eating_habit|
  json.partial! 'miscellaneous_details/eating_habit', eating_habit: eating_habit
end

json.fitness_activities @fitness_activities do |fitness_activity|
  json.partial! 'miscellaneous_details/fitness_activity', fitness_activity: fitness_activity
end