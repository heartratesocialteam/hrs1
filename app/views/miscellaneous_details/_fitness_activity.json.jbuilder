json.id fitness_activity.id
json.name fitness_activity.name
json.created_at fitness_activity.created_at
json.updated_at fitness_activity.updated_at
if fitness_activity.image.present?
  json.image do
    json.avatar_file_name fitness_activity.image.avatar_file_name
    json.avatar_content_type fitness_activity.image.avatar_content_type
    json.created_at fitness_activity.image.created_at
    json.updated_at fitness_activity.image.updated_at
    json.urls fitness_activity.image.urls
  end
else
  json.image fitness_activity.image
end