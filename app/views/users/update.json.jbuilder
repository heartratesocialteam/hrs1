if @status
  json.partial! 'users/user', user_obj: @user, message: 'Profile updated successfully!'
else
  json.partial! 'users/error', user_obj: @user, message: "Profile not updated due to #{@user.errors.full_messages.to_sentence}"
end