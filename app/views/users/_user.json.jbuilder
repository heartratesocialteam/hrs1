json.user do
  json.(user_obj, :id, :first_name, :last_name, :email, :username, :dob, :about, :gender, :occupation,
      :relationship_status, :address, :created_at, :updated_at)
  json.images user_obj.images, :id, :avatar_file_name, :avatar_content_type, :created_at, :urls
  json.followers_count user_obj.followers.count
  json.following_count user_obj.following.count
  json.refresh_token user_obj.refresh_token if defined? auth_token
  json.auth_token auth_token if defined? auth_token
  json.preferences do
    json.(user_obj, :distance)
    json.(user_obj, :age_range)
    json.(user_obj, :purpose)
    json.(user_obj, :dating)
    json.eating_habits user_obj.eating_habits do |eating_habit|
      json.partial! 'miscellaneous_details/eating_habit', eating_habit: eating_habit
    end
    json.fitness_activities user_obj.fitness_activities do |fitness_activity|
      json.partial! 'miscellaneous_details/fitness_activity', fitness_activity: fitness_activity
    end
  end
  json.is_profile_completed user_obj.profile_completed?
end
json.message message if defined? message
