json.partial! 'users/user',
              user_obj: @user,
              message: 'Auth token and Refresh token renewed successfully!',
              auth_token: @user.generate_auth_token
