json.partial! 'users/user',
              user_obj: @user,
              message: @user.new_record? ? 'User created successfully!' : 'User logged in successfully!',
              auth_token: @user.generate_auth_token