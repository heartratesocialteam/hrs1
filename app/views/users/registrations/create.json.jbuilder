if @user.valid?
  json.partial! 'users/user', user_obj: @user, message: 'Profile updated successfully!',
                auth_token: @user.generate_auth_token
else
  # json.partial! 'users/error', user_obj: @user, message: 'Profile not created!'
  json.partial! 'users/error', user_obj: @user, message: "Profile not created due to #{@user.errors.full_messages.to_sentence}"
end