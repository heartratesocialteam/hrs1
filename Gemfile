source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# --------------------------------------------------------- #
# Note -  Gems name's here should be alphabetically ordered.
# --------------------------------------------------------- #

gem 'activeadmin', github: 'activeadmin'  #-> For Admin panel of the application
gem 'active_admin_theme'                  #-> For a cleaner theme
gem 'coffee-rails', '~> 4.2'
gem 'dalli'                   #-> For Memcache management
gem 'devise'                  #-> For User Authentication
gem 'exception_notification'  #-> For exceptions notifications
gem 'geocoder'
# gem 'grocer'                  #-> For push notifications
gem 'google_drive', '~> 1.0.5'
gem 'httparty'
gem 'jbuilder', '~> 2.5'      #-> For better JSON responses
gem 'kaminari'                #-> For Pagination of records
# gem 'koala'
gem 'mina'                    #-> For Deployment
gem 'mysql2'
gem 'paperclip', '~> 5.0.0'   #-> For images of different resources
gem 'puma', '~> 3.7'
gem 'rails', '~> 5.1.2'
gem 'sass-rails', '~> 5.0'
gem 'slack-notifier'          #-> For exceptions notifications
gem 'turbolinks', '~> 5'
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'uglifier', '>= 1.3.0'

# gem 'noto', github: 'aashishgarg/noto', :branch => 'master'

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'capybara', '~> 2.13'
  gem 'factory_girl_rails'    #-> For replacement of fixtures
  gem 'ffaker'                #-> For faking real data
  gem 'letter_opener'         #-> For Emails in development environment
  gem 'rspec-rails', '~> 3.5' #-> For Unit test with TDD
  gem 'selenium-webdriver'    #-> For Browser automation with BDD
end