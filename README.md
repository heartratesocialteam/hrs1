## HeartRateSocial
HeartRateSocial is a social networking application. Here user can define his/her eating habits and fitness activities.
Then user can find his/her network based upon the preference settings which include
 1. Radius under which matches should be found.
 2. Matches of a particular gender.
 3. Age range of people to be found for matches.
 4. Matches having same type of interests.

#### Features
1. User can create profile.
2. User can set preferences for the matches.
3. User can write posts.
4. User can view the posts of its matches(other users).
5. User can like the profile of other user.
6. User can like the post of other user.
7. User can block the other user.
8. User can report the other user.

##### Project Backend code start date - (25-Jul-2017)

#### Technical details
1. Ruby version used is _**2.4.1**_.
2. Rails version used is _**5.1.2**_.
3. Database used is _**mysql**_.
4. Unit tests are written in _**rspec**_.
5. Deployment script is written in _**Mina**_.
6. Admin Application is in _**ActiveAdmin**_.