class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications do |t|
      t.integer :user_id
      t.text :user_object

      t.string :target_type
      t.integer :target_id
      t.text :target_object

      t.string :notify_type

      t.text :message
      t.datetime :read_at

      t.timestamps
    end
  end
end
