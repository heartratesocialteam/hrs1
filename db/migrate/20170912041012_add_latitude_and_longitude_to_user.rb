class AddLatitudeAndLongitudeToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :latitude, :float, after: :purpose
    add_column :users, :longitude, :float, after: :latitude
  end
end
