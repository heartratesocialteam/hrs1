class AddNotifyIdToNotification < ActiveRecord::Migration[5.1]
  def change
    add_column :notifications, :notify_action, :string, after: :notify_type
    add_column :notifications, :notify_id, :string, after: :notify_action
    add_column :notifications, :notify_object, :text, after: :notify_id
  end
end
