class CreateFilters < ActiveRecord::Migration[5.1]
  def change
    create_table :filters do |t|
      t.references :user, foreign_key: true
      t.text :location
      t.text :age_range
      t.text :dating
      t.text :purpose
      t.decimal :distance

      t.timestamps
    end
  end
end
