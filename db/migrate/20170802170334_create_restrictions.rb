class CreateRestrictions < ActiveRecord::Migration[5.1]
  def change
    create_table :restrictions do |t|
      t.integer :blocker_id
      t.integer :blocked_id

      t.timestamps
    end
    add_index :restrictions, :blocker_id
    add_index :restrictions, :blocked_id
    add_index :restrictions, [:blocker_id, :blocked_id], unique: true
  end
end
