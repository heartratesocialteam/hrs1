class AlterDistanceColumnInUsers < ActiveRecord::Migration[5.1]
  def change
    change_column :users, :distance, :decimal, precision: 8, scale: 2, default: 2000
  end
end