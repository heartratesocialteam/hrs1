class DeviseCreateUsers < ActiveRecord::Migration[5.1]
  def self.up
    create_table :users do |t|
      ## Database authenticatable
      t.string :email
      t.string :username
      t.string :facebook_id
      t.string :facebook_token
      t.string :refresh_token

      t.string :first_name
      t.string :last_name
      t.datetime :dob
      t.text :about
      t.string :gender, default: 'male'
      t.string :occupation
      t.text :age_range
      t.decimal :distance, precision: 8, scale: 2, default: 50.0
      t.text :dating
      t.string :relationship_status, default: 'single'
      t.text :purpose
      t.text :address

      t.string :encrypted_password, null: false, default: ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip
      t.timestamps null: false
    end

    add_index :users, :email
    add_index :users, :reset_password_token, unique: true
    # add_index :users, :confirmation_token,   unique: true
    # add_index :users, :unlock_token,         unique: true
  end

  def self.down
    # By default, we don't want to make any assumption about how to roll back a migration when your
    # model already existed. Please edit below which fields you would like to remove in this migration.
    raise ActiveRecord::IrreversibleMigration
  end
end
