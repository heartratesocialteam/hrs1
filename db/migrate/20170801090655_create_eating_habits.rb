class CreateEatingHabits < ActiveRecord::Migration[5.1]
  def change
    create_table :eating_habits do |t|
      t.string :name
      t.timestamps
    end

    create_table :eating_habits_users, id: false do |t|
      t.belongs_to :eating_habit, index: true
      t.belongs_to :user, index: true
    end
  end
end
