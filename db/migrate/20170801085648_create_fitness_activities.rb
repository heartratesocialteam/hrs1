class CreateFitnessActivities < ActiveRecord::Migration[5.1]
  def change
    create_table :fitness_activities do |t|
      t.string :name
      t.timestamps
    end

    create_table :fitness_activities_users, id: false do |t|
      t.belongs_to :fitness_activity, index: true
      t.belongs_to :user, index: true
    end
  end
end
