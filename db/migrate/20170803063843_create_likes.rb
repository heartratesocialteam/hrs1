class CreateLikes < ActiveRecord::Migration[5.1]
  def change
    create_table :likes do |t|
      t.string :likable_type
      t.integer :likable_id
      t.integer :liker_id

      t.timestamps
    end
  end
end