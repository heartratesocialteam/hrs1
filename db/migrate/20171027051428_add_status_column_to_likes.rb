class AddStatusColumnToLikes < ActiveRecord::Migration[5.1]
  def change
    add_column :likes, :status, :integer, default: 0, after: :liker_id
  end
end