unless AdminUser.where(email: 'admin@example.com').present?
  AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')
end

puts '***************CREATING FITNESS ACTIVITIES ***************'
['Crossfit', 'BeachBody', 'Yoga', 'Water Sports', 'Volleyball', 'Tennis', 'Studio Classes', 'Swimming', 'Snow Sports',
 'Lifting', 'Body Building', 'Team Sports', 'Biking / Spin', 'Martial Arts', 'Outdoor', 'Dancing', 'Running',
 'Sedentary'].each do |habit|
  unless FitnessActivity.where(name: habit).present?
    FitnessActivity.create(name: habit)
    puts "***********#{habit}********* CREATED"
  end
end

puts '***************CREATING EATING HABITS ********************'
['GF', 'Keto', 'Paleo', 'Vegetarian', 'Vegan', 'Whole 30', 'Mediterranean', 'Atkins', 'Caffeine', 'Alcohol', 'Smoke',
 'Just Healthy', 'Not Healthy'].each do |habit|
  unless EatingHabit.where(name: habit).present?
    EatingHabit.create(name: habit)
    puts "***********#{habit}********* CREATED"
  end
end

