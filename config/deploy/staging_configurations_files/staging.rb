# ========== Instance Details=============== #
set :host_ip, '34.199.98.25'
set :domain, fetch(:host_ip)
# ========================================== #

# ===============Rails Environment ========= #
set :rails_env, 'staging'
set :ssl_enabled, false
# ========================================== #