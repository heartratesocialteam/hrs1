Rails.application.routes.draw do

#  root to: 'admin/dashboard#index'
  root to: 'home#index'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_for :users, controllers: {registrations: 'users/registrations', sessions: 'users/sessions'}
  devise_scope :user do
    post 'users/sign_up', to: 'users/registrations#create'
    post 'users/renew_auth_token', to: 'users/sessions#renew_auth_token'
  end

  resources :miscellaneous_details, only: [:index]
  resources :users, only: [:update, :show] do
    member do
      get 'matches'

      get 'followers', to: 'relationships#followers'
      get 'following', to: 'relationships#following'

      get 'blockers', to: 'restrictions#blockers'
      get 'blocking', to: 'restrictions#blocking'
    end

    post 'follow', to: 'relationships#create'
    delete 'follow', to: 'relationships#destroy'

    post 'block', to: 'restrictions#create'
    delete 'block', to: 'restrictions#destroy'

    get 'likers', to: 'likes#likers'
    post 'likes', to: 'likes#create'
    delete 'likes', to: 'likes#destroy'

    get 'filter', to: 'filters#show'
    post 'filters', to: 'filters#create'
    delete 'filters', to: 'filters#destroy'

    resources :notifications, only: [:update, :index]
    resources :reports, only: [:create]

    resources :posts, only: [:create, :index, :destroy, :show] do
      post 'likes', to: 'likes#create'
      delete 'likes', to: 'likes#destroy'
    end
  end
end