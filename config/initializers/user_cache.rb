require 'dalli'
require 'singleton'

class UserCacheException < Exception
  def initialize(msg='Cache is already full')
    super(msg)
  end
end

class UserCache
  include Singleton

  def initialize
    options = YAML.load_file(File.join(Rails.root, 'config', 'memcache_settings.yml'))[Rails.env]
    host = options.delete('host')
    port = options.delete('port')
    @connection ||= Dalli::Client.new("#{host}:#{port}", options)
  end

  def get_user_id_from_token(token)
    @connection.get(token)
  end

  def set_token(token, user_id)
    @connection.set(token, user_id, 30.days)
  end

  def set_token_optimistically(token, user_id)
    raise UserCacheException, "Cache is already full with #{token}" unless get_user_id_from_token(token).nil?
    set_token(token, user_id)
  end

  def delete_token(token)
    @connection.delete(token)
  end

end