# require 'singleton'
#
# class UserNotification
#   include Singleton
#
#   def initialize
#     options = YAML.load_file(File.join(Rails.root, 'config', 'notification.yml'))[Rails.env]
#     passphrase = options.delete('passphrase')
#     gateway = options.delete('gateway')
#     port = options.delete('port')
#     retries = options.delete('retries')
#     _pem_file = options.delete('pem_file')
#     @pusher ||= Grocer.pusher(
#         certificate: _pem_file,
#         passphrase: passphrase,
#         gateway: gateway,
#         port: port,
#         retries: retries
#     )
#   end
#
#   def send_notification(options)
#     puts "------ sending notification on #{options[:device_token]} -----------"
#     notification = Grocer::Notification.new(
#         device_token: options[:device_token],
#         alert: options[:message],
#         badge: 0,
#         sound: 'siren.aiff'
#     )
#     @pusher.push(notification)
#   end
#
# end