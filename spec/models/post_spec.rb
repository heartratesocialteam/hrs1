require 'rails_helper'

RSpec.describe Post, type: :model do
  it 'must belong to a user' do
    p = Post.create(content: 'post content only.')
    expect(p.valid?).to eq(false)
  end

  it 'should be valid for a user' do
    u = User.create(email: 'a@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    p = Post.create(content: 'post content only.', user: u)
    expect(p.valid?).to eq(true)
  end

  it 'cannot be less than 2 in length' do
    u = User.create(email: 'a@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    p = Post.create(content: 'p', user: u)
    expect(p.valid?).to eq(false)
  end

  it 'cannot be more than 140 in length' do
    u = User.create(email: 'a@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    p = Post.create(content: 'p'*141, user: u)
    expect(p.valid?).to eq(false)
  end
end
