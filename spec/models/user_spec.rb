require 'rails_helper'

RSpec.describe User, type: :model do
  context 'email' do
    it 'cannot be nil' do
      User.create(email: nil, password: 'Welcome@123', password_confirmation: 'Welcome@123')
      expect(User.count).to eq(0)
    end

    it 'can be nil with skip_email_validation = true' do
      User.create(email: nil, password: 'Welcome@123', password_confirmation: 'Welcome@123', skip_email_validation: true)
      expect(User.count).to eq(1)
    end

    it 'cannot be duplicate' do
      user1 = User.create(email: 'ashish@ashish.com', skip_password_validation: true, skip_email_validation: true)
      user2 = User.new(email: 'ashish@ashish.com', skip_password_validation: true, skip_email_validation: true)
      expect(user2.valid?).to eq(false)
    end

    it 'can be nil' do
      User.create(email: 'a@a.com',username: '', password: 'Welcome@123')
      expect(User.count).to eq(1)
    end

    it 'cannot be duplicate' do
      a = User.create(email: 'a@a.com',username: 'aaa', password: 'Welcome@123')
      b = User.new(email: 'a@a.com',username: 'aaa', password: 'Welcome@123')
      expect(a.valid?).to eq(true)
      expect(b.valid?).to eq(false)
    end
  end
end
