require 'rails_helper'

RSpec.describe Restriction, type: :model do
  it 'blocker id cannot be nil' do
    a = User.create(email: 'a@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    b = Restriction.create(blocker_id: nil, blocked_id: a.id)
    expect(b.valid?).to eq(false)
    expect(Restriction.count).to eq(0)
  end

  it 'blocking id cannot be nil' do
    a = User.create(email: 'a@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    b = Restriction.create(blocker_id: a.id, blocked_id: nil)
    expect(b.valid?).to eq(false)
    expect(Restriction.count).to eq(0)
  end

  it 'blocking id and blocker id cannot be same' do
    a = User.create(email: 'a1@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    b = Restriction.create(blocker_id: a.id, blocked_id: a.id)
    expect(b.valid?).to eq(false)
    expect(Restriction.count).to eq(0)
  end

  it 'blocking id and blocker id should be different' do
    a = User.create(email: 'a@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    b = User.create(email: 'b@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')

    c = Restriction.create(blocker_id: a.id, blocked_id: b.id)
    expect(c.valid?).to eq(true)
    expect(Restriction.count).to eq(1)
  end
end
