require 'rails_helper'

RSpec.describe EatingHabit, type: :model do
  context 'name' do
    it 'cannot be nil' do
      a = EatingHabit.create(name: nil)
      expect(EatingHabit.count).to eq(0)
      expect(a.valid?).to eq(false)
    end
    it 'cannot be blank' do
      a = EatingHabit.create(name: '')
      expect(EatingHabit.count).to eq(0)
      expect(a.valid?).to eq(false)
    end
    it 'cannot be less than 2 in length' do
      a = EatingHabit.create(name: 'a')
      expect(EatingHabit.count).to eq(0)
      expect(a.valid?).to eq(false)
    end
    it 'cannot be more than 50 in length' do
      a = EatingHabit.create(name: 'a'*51)
      expect(EatingHabit.count).to eq(0)
      expect(a.valid?).to eq(false)
    end
    it 'cannot be duplicate' do
      a = EatingHabit.create(name: 'abc')
      b = EatingHabit.create(name: 'abc')
      expect(EatingHabit.count).to eq(1)
      expect(a.valid?).to eq(true)
      expect(b.valid?).to eq(false)
    end
  end
end
