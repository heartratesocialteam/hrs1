require 'rails_helper'

RSpec.describe Like, type: :model do
  it 'should be unique per user for a POST' do
    _liker = User.create(email: 'a@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    _post_user = User.create(email: 'b@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    _p = Post.create(content: 'asdasdas', user_id: _post_user.id)

    l = Like.create(likable: _p, liker_id: _liker.id)
    l1 = Like.create(likable: _p, liker_id: _liker.id)

    expect(l.valid?).to eq(true)
    expect(l1.valid?).to eq(false)
    expect(Like.count).to eq(1)
  end

  it 'should be unique per user for a User' do
    _liker = User.create(email: 'a@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    _user = User.create(email: 'b@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')

    l = Like.create(likable: _user, liker_id: _liker.id)
    l1 = Like.create(likable_type: _user, liker_id: _liker.id)
    expect(l.valid?).to eq(true)
    expect(l1.valid?).to eq(false)
    expect(Like.count).to eq(1)
  end

  it 'cannot like own POST' do
    _liker = User.create(email: 'a@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    _p = Post.create(content: 'asdasdas', user_id: _liker.id)

    l = Like.create(likable: _p, liker_id: _liker.id)
    expect(l.valid?).to eq(false)
    expect(Like.count).to eq(0)
  end

  it 'cannot like own profile' do
    _liker = User.create(email: 'a@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')

    l = Like.create(likable: _liker, liker_id: _liker.id)
    expect(l.valid?).to eq(false)
    expect(Like.count).to eq(0)
  end

end
