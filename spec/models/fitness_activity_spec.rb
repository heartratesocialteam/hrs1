require 'rails_helper'

RSpec.describe FitnessActivity, type: :model do
  context 'name' do
    it 'cannot be nil' do
      a = FitnessActivity.create(name: nil)
      expect(FitnessActivity.count).to eq(1)
      expect(a.valid?).to eq(false)
    end
    it 'cannot be blank' do
      a = FitnessActivity.create(name: '')
      expect(FitnessActivity.count).to eq(0)
      expect(a.valid?).to eq(false)
    end
    it 'cannot be less than 2 in length' do
      a = FitnessActivity.create(name: 'a')
      expect(FitnessActivity.count).to eq(0)
      expect(a.valid?).to eq(false)
    end
    it 'cannot be more than 50 in length' do
      a = FitnessActivity.create(name: 'a'*51)
      expect(FitnessActivity.count).to eq(0)
      expect(a.valid?).to eq(false)
    end
    it 'cannot be duplicate' do
      a = FitnessActivity.create(name: 'abc')
      b = FitnessActivity.create(name: 'abc')
      expect(FitnessActivity.count).to eq(1)
      expect(a.valid?).to eq(true)
      expect(b.valid?).to eq(false)
    end
  end
end
