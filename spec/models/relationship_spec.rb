require 'rails_helper'

RSpec.describe Relationship, type: :model do
  it 'follower cannot be nil' do
    a = User.create(email: 'a@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    r = Relationship.create(follower_id: nil, followed: a)
    expect(r.valid?).to eq(false)
    expect(Relationship.count).to eq(0)
  end

  it 'following cannot be nil' do
    a = User.create(email: 'a@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    r = Relationship.create(follower_id: a, followed: nil)
    expect(r.valid?).to eq(false)
    expect(Relationship.count).to eq(0)
  end

  it 'follower and following cannot be the same' do
    a = User.create(email: 'a@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    r = Relationship.create(follower_id: a, followed: a)
    expect(r.valid?).to eq(false)
    expect(Relationship.count).to eq(0)
  end

  it 'follower and following should be different' do
    a = User.create(email: 'a@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    b = User.create(email: 'b@a.com', password: 'Welcome@123', password_confirmation: 'Welcome@123')
    r = Relationship.create(follower_id: a.id, followed_id: b.id)
    expect(r.valid?).to eq(true)
    expect(Relationship.count).to eq(1)
  end
end
