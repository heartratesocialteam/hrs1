require 'rails_helper'

RSpec.describe EatingHabitsController, type: :controller do
  describe 'GET #index' do
    it 'renders json' do
      get :index
      expect(JSON.parse(response.body)).to include('eating_habits')
    end

    it 'renders json key should be an array' do
      get :index
      expect(JSON.parse(response.body)['eating_habits']).to be_an(Array)
    end

    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
    end
  end
end