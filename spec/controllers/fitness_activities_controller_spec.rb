require 'rails_helper'

RSpec.describe FitnessActivitiesController, type: :controller do
  describe 'GET #index' do
    it 'renders json' do
      get :index
      expect(response.content_type).to include('application/json')
    end

    it 'renders json key should be an array' do
      get :index
      expect(JSON.parse(response.body)['fitness_activities']).to be_an(Array)
    end

    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
    end
  end
end
