FactoryGirl.define do
  factory :notification do
    user_id 1
    actor_id 1
    target_type "MyString"
    target_id 1
    notify_type "MyString"
  end
end
