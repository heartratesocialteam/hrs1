FactoryGirl.define do
  factory :user do
    email FFaker::Internet.email
    username FFaker::Internet.email
    password 'Welcome@123'
    password_confirmation 'Welcome@123'
    first_name FFaker::Name.name
    last_name FFaker::Name.name
    dob '2011-08-08'
    about FFaker::Address.city
    gender 'm'
    occupation 'Business'
    age_range [20, 30]
    distance 20
    dating 'women'
  end
end
