FactoryGirl.define do
  factory :post do
    content 'My post content'
    user {create(:user)}
  end
end
