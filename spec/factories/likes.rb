FactoryGirl.define do

  factory :like do

  end

  factory :profile_like, parent: :like do
    likable {create(:user)}
    liker_id {create(:user).id}
  end

  factory :post_like, parent: :like do
    likable {create(:post)}
    liker_id {create(:user).id}
  end

end
